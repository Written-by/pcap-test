all: pcap-test

pcap-test:  main.o
	g++ -o pcap-test main.o -lpcap

main.o: print.h main.cpp
	g++ -c -o main.o main.cpp

clean:
	rm -f *.o
	rm -f pcap-test
