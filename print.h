#pragma once
#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include <netinet/ether.h>
#include <libnet.h>

typedef struct libnet_ethernet_hdr eth_hdr;
typedef struct libnet_ipv4_hdr ipv4_hdr;
typedef struct libnet_tcp_hdr tcp_hdr;

void print_eth(eth_hdr* eth){
	printf("\nEthernet Header: \n");
	printf("-src mac: %s\n", ether_ntoa((const struct ether_addr*)eth->ether_shost));
	printf("-dst mac: %s\n", ether_ntoa((const struct ether_addr*)eth->ether_dhost));
	return;
}

void print_ip(ipv4_hdr* ip){
	printf("\nIP Header: \n");
	printf("-src ip: %s\n", inet_ntoa((struct in_addr)ip->ip_src));
	printf("-dst ip: %s\n", inet_ntoa((struct in_addr)ip->ip_dst));
	return;
}

void print_tcp(tcp_hdr* tcp){
	printf("\nTCP Header: \n");
	printf("-src port: %u\n", ntohs(tcp->th_sport));
	printf("-dst port: %u\n", ntohs(tcp->th_dport));
	return;
}

void print_payload(eth_hdr* eth, ipv4_hdr* ip, tcp_hdr* tcp){
	printf("\nPayload(Data): \n");
	printf("-hexadecimal value(up to 10 bytes): ");
	uint8_t* payload=(uint8_t*)tcp+((uint8_t)tcp->th_off<<2);
	uint16_t size=ntohs(ip->ip_len)-((uint16_t)ip->ip_hl<<2)-((uint16_t)tcp->th_off<<2);
	if(size>10) size=10;
	for(uint16_t i=0; i<size; i++) printf("|%x", payload[i]);
	printf("|\n");
	printf("-----------------------------------------------------------------------------\n");
	return;
}

